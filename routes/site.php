<?php

/*
|--------------------------------------------------------------------------
| Site Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('/asd', 'StaticController@getStaticPage');

Route::get('/categories', 'Admin\CategoryController@getCategoryList');
Route::get('/category', 'Admin\CategoryController@show');
Route::post('/save-category', 'Admin\CategoryController@saveCategory');

Route::get('/attributes', 'Admin\AttributeController@getAttributeList');
Route::get('/attribute', 'Admin\AttributeController@show');
Route::post('/save-attribute', 'Admin\AttributeController@saveAttribute');*/