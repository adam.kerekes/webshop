@extends('/admin/layout')
@section('content')

    <div>
        <h2>@if($category)"{{$category->name}}" kategória szerkesztése @else Kategória létrehozása @endif </h2>

        <form method="post" action="/save-category" enctype="multipart/form-data">
            @if($category)
                <input type="hidden" name="category_id" value="{{$category->category_id}}" >
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Kategória név" name="name" @if($category) value="{{$category->name}}" @endif>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Többesszám" name="plural_name" @if($category) value="{{$category->plural_name}}" @endif>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Egyedüli név" name="singular_name" @if($category) value="{{$category->singular_name}}" @endif>
                    </div>

                    <div class="input-group mb-3">
                        <label>
                            <select class="custom-select" name="status">
                                <option value="active" @if($category && $category->status === 'active' ) selected="selected" @endif>Aktív</option>
                                <option value="deleted" @if($category && $category->status === 'deleted' ) selected="selected" @endif>Törölt</option>
                                <option value="inactive" @if($category && $category->status === 'inactive' ) selected="selected" @endif>Inaktív</option>
                            </select>
                        </label>
                    </div>

                    <div class="input-group mb-3">
                        <textarea class="form-control" placeholder="Leírás" name="description">@if($category){{$category->description}}@endif</textarea>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="input-group mb-3">
                        @if($category && $category->image_url)
                            <img src="{{$category->image_url}}" height="100" alt="asd">
                        @endif
                        <input type="file" name="category_image">
                    </div>
                </div>
            </div>

            <button class="btn btn-primary" type="submit">@if($category) Mentés @else Kategória létrehozása @endif</button>
            @csrf
        </form>
    </div>

@stop