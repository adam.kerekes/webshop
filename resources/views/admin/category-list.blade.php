@extends('/admin/layout')
@section('content')

    <div class="input-group mb-3">
        <a href="/category">
            <button class="btn btn-primary" type="submit">Kategória létrehozása</button>
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Név</th>
                        <th>Státusz</th>
                        <th>Leírás</th>
                        <th>Szerkesztés</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->category_id}}</td>
                            <td>{{$category->name}}</td>
                            <td>{{$category->status}}</td>
                            <td>{{$category->description}}</td>
                            <td>
                                <a href="/category?category-id={{$category->category_id}}" target="_blank">
                                    <button class="btn btn-primary" type="submit">
                                        Szerkesztés <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop