@extends('/admin/layout')
@section('content')

    <div class="input-group mb-3">
        <a href="/attribute">
            <button class="btn btn-primary" type="submit">Tulajdonság létrehozása</button>
        </a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Név</th>
                    <th>Státusz</th>
                    <th>Leírás</th>
                    <th>Szerkesztés</th>
                </tr>
                </thead>
                <tbody>
                @foreach($attributes as $attribute)
                    <tr>
                        <td>{{$attribute->attribute_id}}</td>
                        <td>{{$attribute->name}}</td>
                        <td>{{$attribute->status}}</td>
                        <td>{{$attribute->description}}</td>
                        <td>
                            <a href="/attribute?attribute-id={{$attribute->attribute_id}}" target="_blank">
                                <button class="btn btn-primary" type="submit">
                                    Szerkesztés <i class="fa fa-pencil" aria-hidden="true"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop