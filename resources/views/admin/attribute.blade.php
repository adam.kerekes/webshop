@extends('/admin/layout')
@section('content')

    <div>
        <h2>@if($attribute)"{{$attribute->name}}" tulajdonság szerkesztése @else Tulajdonság létrehozása @endif </h2>

        <form method="post" action="/save-attribute">
            @if($attribute)
                <input type="hidden" name="attribute_id" value="{{$attribute->attribute_id}}" >
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Tulajdonság név" name="name" @if($attribute) value="{{$attribute->name}}" @endif>
                    </div>

                    <div class="input-group mb-3">
                        <label>
                            <select class="custom-select" name="status">
                                <option value="active" @if($attribute && $attribute->status === 'active' ) selected="selected" @endif>Aktív</option>
                                <option value="deleted" @if($attribute && $attribute->status === 'deleted' ) selected="selected" @endif>Törölt</option>
                                <option value="inactive" @if($attribute && $attribute->status === 'inactive' ) selected="selected" @endif>Inaktív</option>
                            </select>
                        </label>
                    </div>

                    <div class="input-group mb-3">
                        <textarea class="form-control" placeholder="Leírás" name="description">@if($attribute){{$attribute->description}}@endif</textarea>
                    </div>
                </div>
            </div>

            <button class="btn btn-primary" type="submit">@if($attribute) Mentés @else Kategória létrehozása @endif</button>
            @csrf
        </form>
    </div>

@stop