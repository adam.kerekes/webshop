<?php

namespace App\Services\Admin;

use App\Models\Attribute;
use App\Models\Dao\Admin\AttributeAdminDao;
use Illuminate\Database\Eloquent\Model;

class AttributeAdminService
{
    public static function getAttribute($attributeId): Model
    {
        return AttributeAdminDao::getAttributeById($attributeId);
    }

    public static function getAllAttribute()
    {
        return AttributeAdminDao::getAllAttribute();
    }

    public static function saveAttribute(Attribute $attribute, $request)
    {
        $attribute->name = $request['name'];
        $attribute->status = $request['status'];
        $attribute->description = $request['description'];

        $attribute->save();
    }
}