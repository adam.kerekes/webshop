<?php

namespace App\Services\Admin;


use App\Models\Dao\Admin\ProductAdminDao;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ProductAdminService
{
    public static function getProduct($productId): Model
    {
        return ProductAdminDao::getProductById($productId);
    }

    public static function getAllProduct()
    {
        return ProductAdminDao::getAllProduct();
    }

    public static function saveProduct(Product $product, $request)
    {
        $product->name = $request['name'];
        $product->status = $request['status'];
        $product->description = $request['description'];

        $product->save();
    }
}