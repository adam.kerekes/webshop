<?php

namespace App\Services\Admin;


use App\Helpers\ImageType;
use App\Models\Category;
use App\Models\Dao\Admin\CategoryAdminDao;
use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Storage;

class CategoryAdminService
{
    public static function getCategory($categoryId): Model
    {
        /** @var Category $category */
        $category = CategoryAdminDao::getCategoryById($categoryId);
        if ($category->image) {
            $category->image_url = $category->image->url();
        }

        return $category;
    }

    public static function getAllCategory()
    {
        return CategoryAdminDao::getAllCategory();
    }

    public static function saveCategory(Category $category, $request)
    {
        $category->name = $request['name'];
        $category->plural_name = $request['plural_name'];
        $category->singular_name = $request['singular_name'];
        $category->status = $request['status'];
        $category->description = $request['description'];

        $category->save();

        $image = null;

        if(isset($request['category_image'])) {
            /** @var UploadedFile $uploadedImage */
            $uploadedImage = $request['category_image'];

            $image = new Image();
            $image->image_type = ImageType::CATEGORY;
            $image->save();

            $savedImageUrl = Storage::disk('public')->putFileAs(
                'images/category',
                $uploadedImage,
                'category-' .$category->category_id. '-' .$image->image_id. '.jpg');

            $image->url = $savedImageUrl;
            $image->save();
        }

        if ($image) {
            $category->image_id = $image->image_id;
        }

        $category->save();
    }
}