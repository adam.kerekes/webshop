<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Services\Admin\CategoryAdminService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CategoryController extends Controller
{
    public function show(Request $request)
    {
        $request = $request->all();

        $category = null;
        if (isset($request['category-id'])) {
            $category = CategoryAdminService::getCategory($request['category-id']);
        }

        return view('admin/category', ['category' => $category]);
    }

    public function saveCategory(Request $request)
    {
        $request = $request->all();

        if (isset($request['category_id'])) {
            $category = CategoryAdminService::getCategory($request['category_id']);
        } else {
            $category = new Category();
        }

        CategoryAdminService::saveCategory($category, $request);

        return redirect('/categories');
    }

    public function getCategoryList(Request $request)
    {
        $categories = CategoryAdminService::getAllCategory();

        return view('admin/category-list', ['categories' => $categories]);
    }
}