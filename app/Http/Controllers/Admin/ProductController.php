<?php

namespace App\Http\Controllers\Admin;


use App\Models\Product;
use App\Services\Admin\ProductAdminService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    public function show(Request $request)
    {
        $request = $request->all();

        $product = null;
        if (isset($request['product-id'])) {
            $product = ProductAdminService::getProduct($request['product-id']);
        }

        return view('admin/product', ['product' => $product]);
    }

    public function saveProduct(Request $request)
    {
        $request = $request->all();

        if (isset($request['product_id'])) {
            $product = ProductAdminService::getProduct($request['product_id']);
        } else {
            $product = new Product();
        }

        ProductAdminService::saveProduct($product, $request);

        return redirect('/products');
    }

    public function getProductList(Request $request)
    {
        $products = ProductAdminService::getAllProduct();

        return view('admin/product-list', ['products' => $products]);
    }
}