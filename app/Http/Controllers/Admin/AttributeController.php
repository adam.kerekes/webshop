<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attribute;
use App\Services\Admin\AttributeAdminService;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function show(Request $request)
    {
        $request = $request->all();

        $attribute = null;
        if (isset($request['attribute-id'])) {
            $attribute = AttributeAdminService::getAttribute($request['attribute-id']);
        }

        return view('admin/attribute', ['attribute' => $attribute]);
    }

    public function saveAttribute(Request $request)
    {
        $request = $request->all();

        if (isset($request['attribute_id'])) {
            $attribute = AttributeAdminService::getAttribute($request['attribute_id']);
        } else {
            $attribute = new Attribute();
        }

        AttributeAdminService::saveAttribute($attribute, $request);

        return redirect('/attributes');
    }

    public function getAttributeList(Request $request)
    {
        $attributes = AttributeAdminService::getAllAttribute();

        return view('admin/attribute-list', ['attributes' => $attributes]);
    }

}