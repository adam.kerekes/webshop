<?php

namespace App\Helpers;

class ImageType
{
    const CATEGORY = 'category';
    const ATTRIBUTE = 'attribute';
    const ATTR_VAL = 'attr_val';
}