<?php

namespace App\Console\Commands;

use App\Models\AttributeValue;
use Illuminate\Console\Command;

class CreateListPageData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:row';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $attrVal = new AttributeValue();
        $attrVal->name = 'Random';
        $attrVal->attribute_id = 1;
        $attrVal->save();
    }
}
