<?php

namespace App\Models;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $product_id
 * @property string $name
 * @property string $description
 * @property int $category_id
 * @property int $primary_image_id
 * @property string $status
 * @property int $price
 * @property int $discount_price
 * @property float $discount_percent
 * @property string $discount_start
 * @property string $discount_end
 * @property int $parent_product_id
 * @property array $addition_product_ids
 * @property int $variation_attribute_1
 * @property int $variation_attribute_2
 * @property array $attribute_values
 * @property array $empty_attributes
 * @property string $created_at
 * @property string $updated_at
 */
class Product extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'public.products';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'product_id';

    public function image()
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'product_id');
    }
}
