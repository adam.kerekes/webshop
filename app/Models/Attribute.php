<?php


namespace App\Models;

/**
 * Class Attribute
 * @package App\Models
 *
 * @property int $attribute_id
 * @property string $name
 * @property string $status
 * @property int $category_ids
 * @property int $filter_category_ids
 * @property string $description
 * @property int $image_id
 */
class Attribute extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'public.attributes';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'attribute_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

}
