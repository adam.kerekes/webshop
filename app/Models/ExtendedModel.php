<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ExtendedModel
 * @package App\Models
 *
 * @mixin \Eloquent
 */
class ExtendedModel extends Model
{

}