<?php

namespace App\Models;

/**
 * Class Category
 * @package App\Models
 *
 * @property int $category_id
 * @property string $name
 * @property string $plural_name
 * @property string $singular_name
 * @property string $status
 * @property string $description
 * @property int $image_id
 * @property int $attributes
 *
 * @property Image $image
 */
class Category extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'public.categories';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'category_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function image()
    {
        return $this->hasOne(Image::class, 'image_id', 'image_id');
    }
}
