<?php

namespace App\Models\Dao\Admin;

use App\Models\Attribute;
use Illuminate\Database\Eloquent\Model;

final class AttributeAdminDao
{
    public static function getAttributeById ($attributeId) : Model
    {
        return Attribute::find($attributeId);
    }

    public static function getAllAttribute ()
    {
        return Attribute::all();
    }
}