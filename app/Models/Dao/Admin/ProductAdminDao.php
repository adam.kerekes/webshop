<?php

namespace App\Models\Dao\Admin;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

final class ProductAdminDao
{
    public static function getProductById ($productId) : Model
    {
        return Product::find($productId);
    }

    public static function getAllProduct ()
    {
        return Product::all();
    }
}