<?php

namespace App\Models\Dao\Admin;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

final class CategoryAdminDao
{
    public static function getCategoryById ($categoryId) : Model
    {
        return Category::find($categoryId);
    }

    public static function getAllCategory ()
    {
        return Category::all();
    }
}