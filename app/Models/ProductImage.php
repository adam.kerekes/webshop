<?php

namespace App\Models;

/**
 * Class ProductImage
 *
 * @package App\Models
 * @property int $product_image_id
 * @property int $product_id
 * @property string $url
 * @property string $real_link
 * @property bool $is_primary
 */
class ProductImage extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'public.product_images';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'product_image_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
