<?php

namespace App\Models;

/**
 * Class Image
 *
 * @package App\Models
 * @property int $image_id
 * @property string $image_type
 * @property string $url
 */
class Image extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'public.images';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'image_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function url()
    {
        return '/storage/' . $this->url;
    }
}
