<?php

namespace App\Models;

/**
 * Class AttributeValue
 * @package App\Models
 *
 * @property int $attr_val_id
 * @property int $attribute_id
 * @property string $name
 * @property string $status
 * @property bool $filterable
 * @property string $description
 * @property int $image_id
 */
class AttributeValue extends ExtendedModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'public.attribute_values';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'attr_val_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
