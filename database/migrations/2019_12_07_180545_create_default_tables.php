<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDefaultTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::beginTransaction();

        $sql = "create type public.any_status as enum ('active', 'deleted', 'inactive')";

        DB::statement($sql);

        $sql = "create table public.products (
                  product_id serial primary key,
                  name text not null,
                  description text default null,
                  category_id int not null,
                  primary_image_id int not null,
                  status any_status default 'active',
                  price int not null,
                  discount_price int default null,
                  discount_percent float default null,
                  discount_start timestamp default null,
                  discount_end timestamp default null,
                  parent_product_id int default null,
                  addition_product_ids int[] default null,
                  variation_dimension_1 int default null,
                  variation_dimension_2 int default null,
                  variation_attribute_1 int default null,--Ezt miért vittem fel 2x más néven?
                  variation_attribute_2 int default null,
                  attribute_values int[] not null,
                  empty_attributes int[] default null,
                  created_at timestamp default now(),
                  updated_at timestamp default null
              )";

        DB::statement($sql);

        $sql = "create table public.categories (
                  category_id serial primary key,
                  name text not null,
                  plural_name text default null,
                  singular_name text default null,
                  status any_status default 'active',
                  description text default null,
                  image_id int default null,
                  attributes int[] default null
              )";

        DB::statement($sql);

        $sql = "create table public.attributes (
                  attribute_id serial primary key,
                  name text not null,
                  status any_status default 'active',
                  category_ids int[] default null,
                  filter_category_ids int[] default null,
                  description text,
                  image_id int default null
              )";

        DB::statement($sql);

        $sql = "create table public.attribute_values (
                  attr_val_id serial primary key,
                  attribute_id int not null,
                  name text not null,
                  status any_status default 'active',
                  filterable bool default true,
                  description text,
                  image_id int default null
              )";

        DB::statement($sql);

        $sql = "create table public.product_images (
                  product_image_id serial primary key,
                  product_id int not null,
                  url text default null,
                  real_link text default null,
                  is_primary bool default false
              )";

        DB::statement($sql);

        $sql = "create type public.any_image_type as enum ('category', 'attribute', 'attr_val')";

        DB::statement($sql);

        $sql = "create table public.images (
                  image_id serial primary key,
                  image_type any_image_type not null,
                  url text default null
              )";

        DB::statement($sql);

        $sql = "create table public.orders (
                  order_id serial primary key,
                  submit_date timestamp not null default now(),
                  --status egyedi type elkészítése
                  first_name text not null,
                  last_name text not null,
                  billing_country text not null,
                  billing_zip_code text not null,
                  billing_city text not null,
                  billing_address text not null
              )";
/*
        DB::statement($sql);

        $sql = "create table public.order_items (
                  image_id serial primary key,
              )";

        DB::statement($sql);

        $sql = "create table public.product_reviews (
                  image_id serial primary key,
              )";

        DB::statement($sql);

        $sql = "create table public.product_data (
                  image_id serial primary key
              )";

        DB::statement($sql);

        $sql = "create table public.list_page_categories (
                  --hamarosan
              )";

        DB::statement($sql);*/

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
